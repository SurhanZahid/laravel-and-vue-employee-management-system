<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Companies;
use Illuminate\Support\Facades\Mail;
use App\Mail\MyMail;

class companiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = Companies::paginate(10);
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|string|min:1|max:255',
            'email' => 'required|unique',
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'website' => 'required|string',
        ]);

        $fileName = time() . '.' . $request->logo->getClientOriginalExtension();

        $request->logo->move(public_path('upload'), $fileName);

        $companie = Companies::create($request->all());

        $details = [

            'title' => 'Mail from XYZ.com',

            'body' => 'Your Company Has Been Registered'

        ];



        Mail::to($request->email)->send(new MyMail($details));

        return response()->json([
            'message' => 'Companie Created Successfully!!',
            'companie' => $companie
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Companies $companie)
    {
        return response()->json($companie);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $companie = Companies::find($id);
        $companie->update($request->all());

        return response()->json([
            'message' => 'Companie Updated Successfully!!',
            'companie' => $request
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Companies $companie)
    {
        $companie->delete();
        return response()->json([
            'message' => 'Companie Deleted Successfully!!'
        ]);
    }
}
